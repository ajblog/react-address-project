import { AddressModel } from "./AddressModel";

export type ActionTypes =
  | { type: "ADD_ADDRESS"; payload: AddressModel }
  | { type: "DELETE_ADDRESS"; payload: number }
  | { type: "UPDATE_ADDRESS"; payload: AddressModel };
