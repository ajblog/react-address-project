export interface AddressModel {
  id: number;
  type: string;
  title: string;
  latitude: number;
  longitude: number;
  description: string;
}
