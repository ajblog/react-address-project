import React from "react";
import styles from "./AddressTable.module.scss";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { AddressModel } from "../../models/AddressModel";
import { ActionTypes } from "../../models/StoreModel";

interface Props {
  Addresses: AddressModel[];
}

const AddressTable: React.FC<Props> = ({ Addresses }) => {
  let deletAction: ActionTypes;
  const dispatch = useDispatch();
  return (
    <table className={styles.AddressTable}>
      <thead>
        <tr>
          <th>Title</th>
          <th>Type</th>
          <th>Description</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        {Addresses.map((item: AddressModel, index: number) => {
          return (
            <tr key={index}>
              <td>{item.title}</td>
              <td>{item.type}</td>
              <td>{item.description}</td>
              <td>
                <Link to={`/address/detail/${item.id}`}>
                  <button className={styles.AddressActionBtn}>
                    <i className="fas fa-edit"></i>
                  </button>
                </Link>
                <button
                  className={styles.AddressActionBtn}
                  onClick={() => {
                    deletAction = { type: "DELETE_ADDRESS", payload: item.id };
                    dispatch(deletAction);
                  }}
                >
                  <i className="far fa-trash-alt"></i>
                </button>
              </td>
            </tr>
          );
        })}
        <tr></tr>
      </tbody>
    </table>
  );
};

export default AddressTable;
