import React from "react";
import styles from "./Welcome.module.scss";

const Welcome: React.FC = () => {
  return (
    <div className={styles.WelcomeContainer}>
      <h1 className={styles.HiHeading}>Hi There :|</h1>
      <p className={styles.BriefDetailsParagraph}>
        Here is a simple project for adding - deleting - updating data(address)
        on the client side...You can find the Code here on my{" "}
        <span>
          <a className={styles.GitLabLink} href="/">
            Gitlab{" "}
          </a>
        </span>
        Account.
        <br />
        P.S: Sorry for the poor styles. I didn't have much time to put on design
        and animation :|
      </p>
      <ul className={styles.BriefDetailList}>
        <li>I've used React with TypeScript</li>
        <li>The components have been styled with Modular Scss</li>
        <li>The Maps Are being handled with Leaflet and React-Leaflet</li>
        <li>And for global state management I've used Redux and React-Redux</li>
      </ul>
    </div>
  );
};

export default Welcome;
