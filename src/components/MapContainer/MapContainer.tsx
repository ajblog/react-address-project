import React, { useEffect, useState, useRef, useMemo } from "react";
import { Marker, Popup, MapContainer as Map, TileLayer } from "react-leaflet";
import styles from "./MapContainer.module.scss";
import L from "leaflet";
import MarkerIcon from "../../assets/img/Map_Marker.png";
import { AddressModel } from "../../models/AddressModel";
import { LatLng } from "../../models/MapModel";

interface Props {
  currentAddress: AddressModel;
  setCurrentAddress: React.Dispatch<React.SetStateAction<AddressModel>>;
}

const setDefaultMapCenter = (address: AddressModel): LatLng => {
  if (address.longitude === -1 && address.longitude === -1)
    return { lat: 35.6892, lng: 51.389 };
  return { lat: address.latitude, lng: address.longitude };
};

const DraggableMarker: React.FC<Props> = ({
  currentAddress,
  setCurrentAddress,
}) => {
  const [position, setPosition] = useState(setDefaultMapCenter(currentAddress));
  useEffect(() => {
    setCurrentAddress({
      ...currentAddress,
      latitude: position.lat,
      longitude: position.lng,
    });
  }, [position]);
  const CustomIcon = new L.Icon({
    iconUrl: MarkerIcon,
    iconSize: [40, 40],
  });

  const markerRef = useRef(null);
  const eventHandlers = useMemo(
    () => ({
      dragend() {
        const marker: any = markerRef.current;
        if (marker != null) {
          setPosition(marker.getLatLng());
        }
      },
    }),
    []
  );

  return (
    <Marker
      draggable={true}
      eventHandlers={eventHandlers}
      position={position}
      ref={markerRef}
      icon={CustomIcon}
    >
      <Popup minWidth={90}>
        <span>{currentAddress.type}</span>
      </Popup>
    </Marker>
  );
};

const MapContainer: React.FC<Props> = ({
  currentAddress,
  setCurrentAddress,
}) => {
  const [latlng, setLatlng] = useState<LatLng>(
    setDefaultMapCenter(currentAddress)
  );
  useEffect(() => {
    setLatlng(setDefaultMapCenter(currentAddress));
  }, [currentAddress]);

  return (
    <Map
      className={styles.Map}
      center={latlng}
      zoom={8}
      scrollWheelZoom={true}
      key={JSON.stringify(latlng)}
    >
      <TileLayer
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <DraggableMarker
        currentAddress={currentAddress}
        setCurrentAddress={setCurrentAddress}
      />
    </Map>
  );
};

export default MapContainer;
