import { AddressModel } from "../models/AddressModel";
import { ActionTypes } from "../models/StoreModel";
const initialState: AddressModel[] = [
  {
    title: "Fatemi Fire Station",
    description: "It will hear you emergency!!!",
    id: 1,
    latitude: 31.6892,
    longitude: 51.389,
    type: "Fire Station",
  },
  {
    title: "Mamad's Shishlik",
    description: "They use the best meat you'll ever taste :)",
    id: 2,
    latitude: 30.6892,
    longitude: 51.389,
    type: "Restaurant",
  },
  {
    title: "Ghorbani School",
    description: "They are one of the worst schools in the country",
    id: 3,
    latitude: 34.6892,
    longitude: 51.389,
    type: "School",
  },
];

const reducer = (
  state: AddressModel[] = initialState,
  action: ActionTypes
): AddressModel[] => {
  let temp_state = state;
  switch (action.type) {
    case "ADD_ADDRESS":
      return [...state, action.payload];
    case "DELETE_ADDRESS":
      temp_state = temp_state.filter((item) => item.id !== action.payload);
      return temp_state;
    case "UPDATE_ADDRESS":
      let updatingElementIndex: number = -1;
      state.forEach((item, index) => {
        if (item.id === action.payload.id) updatingElementIndex = index;
      });
      temp_state.splice(updatingElementIndex, 1, action.payload);
      return temp_state;
    default:
      return state;
  }
};

export default reducer;
