import data from "../assets/data/addressData.json";

export const addressTypesData = () => {
  return data.addressTypes;
};
