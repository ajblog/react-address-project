import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import AddressDetail from "./pages/AddressDetail/AddressDetail";
import Home from "./pages/Home/Home";
import NotFound from "./pages/NotFound/NotFound";

const Router: React.FC = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/address/detail/:id" component={AddressDetail} />
        <Route exact path="/address/new" component={AddressDetail} />
        <Route component={NotFound} />
      </Switch>
    </BrowserRouter>
  );
};

export default Router;
