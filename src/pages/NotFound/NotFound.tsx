import React from "react";
import styles from "./NotFound.module.scss";
import { Link } from "react-router-dom";

const NotFound: React.FC = () => {
  return (
    <div className={styles.Container}>
      <h1 className={styles.ErrorTitle}>Sorry Not Found :/</h1>
      <p className={styles.ErrorDescription}>
        The Url you requesting doesn't exist in this application!
      </p>
      <Link className={styles.GoBackLink} to="/">
        GO BACK!
      </Link>
    </div>
  );
};

export default NotFound;
