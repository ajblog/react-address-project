import React, { useState, useLayoutEffect } from "react";
import { Link, RouteComponentProps, useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import MapContainer from "../../components/MapContainer/MapContainer";
import { AddressModel } from "../../models/AddressModel";
import { addressTypesData } from "../../utils/readFromAssets";
import styles from "./AddressDetail.module.scss";
import { ActionTypes } from "../../models/StoreModel";

const AddressDetail: React.FC<RouteComponentProps> = ({ match }) => {
  let history = useHistory();
  const [currentAddress, setCurrentAddress] = useState<AddressModel>({
    id: -1,
    type: "Hospital",
    title: "",
    latitude: -1,
    longitude: -1,
    description: "",
  });

  const [isEditMode, setIsEditMode] = useState<boolean>(false);
  const store = useSelector((state: AddressModel[]) => state);
  const dispatch = useDispatch();
  useLayoutEffect((): void => {
    if (Object.keys(match.params).length) {
      setIsEditMode(true);
      setCurrentAddress(
        getEditableAddress(`${Object.values(match.params)[0]}`)
      );
    }
  }, []);

  const submitAddress = (): void => {
    let addEditAction: ActionTypes;
    if (!checkIfAddressIsValid()) {
      alert("Please Submit a Proper Address!");
      return;
    }
    if (!isEditMode) {
      computeNewUniqueId();
      addEditAction = {
        type: "ADD_ADDRESS",
        payload: { ...currentAddress, id: computeNewUniqueId() },
      };
      dispatch(addEditAction);
      alert("Address Successfully added!");
      history.push("/");
      return;
    }
    addEditAction = { type: "UPDATE_ADDRESS", payload: currentAddress };
    dispatch(addEditAction);
    alert("Address Successfully updated!");
    history.push("/");
  };

  const checkIfAddressIsValid = (): boolean => {
    const isLatValid: boolean = currentAddress.latitude !== -1;
    const isLngvalid: boolean = currentAddress.longitude !== -1;
    const isTitleValid: boolean = currentAddress.title.length > 0;
    const isDesValid: boolean = currentAddress.description.length > 0;
    return isLatValid && isLngvalid && isTitleValid && isDesValid;
  };

  const getEditableAddress = (paramsId: string): AddressModel => {
    let editableAddress: AddressModel = {
      type: "",
      description: "",
      title: "",
      longitude: -1,
      latitude: -1,
      id: -1,
    };
    store.forEach((item) => {
      if (item.id === +paramsId) editableAddress = item;
    });
    if (editableAddress.id === -1) history.push("/not-found");
    return editableAddress;
  };

  const computeNewUniqueId = (): number => {
    let newId: number = 0;
    newId = store[store.length - 1].id + 1;
    return newId;
  };

  return (
    <div className={styles.Container}>
      <h1 className={styles.PageTitle}>
        {isEditMode ? "Edit " : "Share "} Location
      </h1>
      <div className={styles.ContainerBody}>
        <div className={styles.SubmitAddressForm}>
          <form>
            <label>Title:</label>
            <input
              onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                setCurrentAddress({ ...currentAddress, title: e.target.value })
              }
              defaultValue={currentAddress.title}
              type="text"
            />
            <label>Type:</label>

            <select
              onChange={(e: React.ChangeEvent<HTMLSelectElement>) =>
                setCurrentAddress({ ...currentAddress, type: e.target.value })
              }
              value={currentAddress.type}
            >
              {addressTypesData().map((item: string, index: number) => {
                return (
                  <option key={index} value={item}>
                    {item}
                  </option>
                );
              })}
            </select>
            <label>Description:</label>
            <textarea
              onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) =>
                setCurrentAddress({
                  ...currentAddress,
                  description: e.target.value,
                })
              }
              defaultValue={currentAddress.description}
              rows={4}
            />
            <div className={styles.ButtonContainer}>
              <Link className={styles.ReturnLink} to="/">
                <button className={styles.ReturnBtn}>Return</button>
              </Link>
              <button
                className={styles.SubmitBtn}
                onClick={() => submitAddress()}
              >
                Submit
              </button>
            </div>
          </form>
        </div>
        <div className={styles.MapContinar}>
          <MapContainer
            currentAddress={currentAddress}
            setCurrentAddress={setCurrentAddress}
          />
        </div>
      </div>
    </div>
  );
};

export default AddressDetail;
