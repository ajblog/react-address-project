import React from "react";
import styles from "./Home.module.scss";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { AddressModel } from "../../models/AddressModel";
import AddressTable from "../../components/AddressTable/AddressTable";
import Welcome from "../../components/Welcome/Welcome";

const Home: React.FC = () => {
  const state = useSelector((state: AddressModel[]) => state);

  return (
    <div className={styles.Container}>
      <Welcome />
      <AddressTable Addresses={state} />
      <Link className={styles.NewAddressButton} to="/address/new">
        Create New Address
      </Link>
    </div>
  );
};

export default Home;
