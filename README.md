# Share Loaction Project

Ali Jafarian - 2021

## Brief Explanation

This project was create by `create-react-app --template typescript`
And it uses the following structure:

### `Utils`

Contains a `.ts`file for reading static data(JSON) in assets folder!

### `store`

Contains of `store.js` and `reducer.js` (Typical Redux structure).

### `Models`

A place to keep our interface that are shared all around the application.

### `Pages`

Here are the 3 main pages that we have for this app, which are: `Home` and `AddressDetail` and `NotFound` for 404.

### `Components`

In the end we have our components. which are typical tsx components.

### `Assets`

Where we keep Our static JSON data and Pictures.
